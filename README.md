# freeze

Minimal project to diagnose a freezing in my Chrome with Nuxt projects.

Procedure to test:
  * Test: yarn dev, and click on Home, Something, Home, Something... after 3-4 clicks it freezes for a few seconds (20-30s)... and after those seconds it continues as usual.

Hints until now
  * Tested only in the laptop of Javi Sedano.
  * With Firefox and Safari it works ok. With Chrome it freezes.
  * With `yarn dev` it freezes, with `yarn build && yarn start`it works ok.
  * In incognito mode, it freezes as well.
  * With Chrome in another computer and browsing http://<yourip>:3001 and repeating the test, it freezes as well.
  * Simplifying the app to the bare minimum (only two pages with two links between them; no dependencies, no layouts, no components, no nothing) it freezes as well.
  * Doing the same test betwen Static A and Static B does not reproduce the error (fortunately, because they are static pages).
  * Doing the test between Something and Static A reproduces the error, but always when going from Something to Static A, and never when going from Static A to Something... so looks like the problem happens when navigating from the Nuxt application to an <a> link.
  * Replacing <a> with <NuxtLink> fixes the problem... but it may not be an aceptable solution in my real use case, because I am creating a Stencil framework-agnostic component.

Video of the freeze here: https://drive.google.com/file/d/1Hfp_1tskppJjqxkZKamaUGeR4JQ55bGI/view?usp=sharing

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
```

